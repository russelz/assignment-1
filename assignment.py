#import random for later
import random

# parent class
class Item():
    def __init__(self,name,kind,size,price,id):
        self.name=name
        self.kind=kind
        self.size=size
        self.price=price
        self.id=id
    def desc_item(self):
        desc_str="Item no. %s - %s is a %s, %s: $"%(self.id,self.name,self.kind,self.size)+format(self.price/100, ',.2f')
        return desc_str

biglist=[*range(1000,10000)]
numlist=random.sample(biglist,22)
num1,num2,num3,num4,num5,num6,num7,num8,num9,num10,num11,num12,num13,num14,num15,num16,num17,num18,num19,num20,num21,num22=str(numlist).replace(","," ").split()

#brush class inherits from item
class Brush(Item):
    pass

# Different Brushes
brush1 = Brush("BRUSH1","Horsehair Brush",".5 in",137,num2)
brush2 = Brush("BRUSH2","Disposable Brush","1 in",107,num3)
brush3 = Brush("BRUSH3","Disposable Brush","2 in",128,num4)
brush4 = Brush("BRUSH4","Disposable Brush","3 in",168,num5)
brush5 = Brush("BRUSH5","Disposable Brush","4 in",422,num6)
brush6 = Brush("BRUSH6","Chiseled Foam Brush","1 in",68,num7)
brush7 = Brush("BRUSH7","Chiseled Foam Brush","2 in",78,num8)
brush8 = Brush("BRUSH8","Chiseled Foam Brush","3 in",97,num9)
brush9 = Brush("BRUSH9","Reusable Brush","1 in",847,num10)
brush10 = Brush("BRUSH10","Reusable Brush","2 in",1197,num11)

# Paint Class inherets from item
class Paint(Item):
    pass

# Different Paints
paint1 = Paint("PAINT1","Blue Paint","1 gal",3500,num12)
paint2 = Paint("PAINT2","Blue Paint","5 gal",6000,num13)
paint3 = Paint("PAINT3","Red Paint","1 gal",3500,num14)
paint4 = Paint("PAINT4","Red Paint","5 gal",6000,num15)
paint5 = Paint("PAINT5","Yellow Paint","1 gal",3500,num16)
paint6 = Paint("PAINT6","Yellow Paint","5 gal",6000,num17)
paint7 = Paint("PAINT7","Green Paint","1 gal",3500,num18)
paint8 = Paint("PAINT8","Green Paint","5 gal",6000,num19)
paint9 = Paint("PAINT9","Black Paint","1 gal",3500,num20)
paint10 = Paint("PAINT10","Black Paint","5 gal",6000,num21)

#items list to pull info from
items = [brush1, brush2, brush3, brush4, brush5, brush6, brush7, brush8, brush9, brush10,
 paint1, paint2, paint3, paint4, paint5, paint6, paint7, paint8, paint9, paint10]

#cart list to be added to
cart=[]

#greeting and instruction for interface menu
print("Welcome to Sal's Paints!")
print("You can view ITEMS, view your CART, ADD items, REMOVE items, CHECKOUT, or get HELP.")

while True:
    #start of interface commands
    command = input("What can we do for you? ").upper()
    
    #help function if person forgets command options
    if command == "HELP":
        print("You can view ITEMS, view your CART, ADD items, REMOVE items, CHECKOUT, or get HELP.")
        continue

    #function to view cart
    if command == "CART":
        print("CART")
        for item in cart:
            print(item.desc_item())
        continue

    #function to view items list, displays item info based on item class          
    if command == "ITEMS":
        print("ITEMS")
        for item in items:
            print(item.desc_item())
        continue
    
    #function to add items to cart list, asks if you would like to add more than one item
    if command == "ADD":
        while True:
            additem = input("What would you like to add? ").upper()
            for item in items:
                if additem == item.name or additem == item.id:
                    cart.append(item)
                    continue
            addmore = input("Would you like to add another item? ").upper()
            if addmore == "YES" or addmore == "Y":
                continue
            if addmore != "YES" or addmore != "Y":
                break
        continue

        #function  to remove items from cart list, asks about multiple items and displays cart
    if command == "REMOVE":
        while True:
            for item in cart:
                print(item.desc_item())
            removeitem=input("What would you like to remove? ").upper()
            for item in cart:
                if removeitem==item.name:
                    cart.remove(item)
                    continue
            removemore=input("Remove another item? ").upper()
            if removemore == "YES" or removemore == "Y":
                continue
            if removemore != "YES" or removemore != "Y":
                break
        continue

    #function to finish shopping and pay, asks for coupons, card, and address and empties cart after finishing        
    if command == "CHECKOUT":
        total = 0
        for item in cart:
            total = total + item.price
        print("Your total before tax is $" + format(total/100,',.2f'))
        while True:
            payment = input("Please enter any coupon codes you have or select CREDIT or DEBIT now: ").upper()
            if payment == "ZRUSS" and total >= 5000:
                total = total - (total*.05)
                print("Your total before tax is now $" + format(total/100,',.2f'))
                continue
            if payment == "ZRUSS" and total <= 5000:
                print("I'm sorry, your total is not high enough to qualify for this offer. Your total must exceed $50.00")
                continue
            if payment == "CREDIT" or payment == "DEBIT":
                while True:
                    card = input("Please enter the 16 digit card number: ")
                    if len(card) != 16:
                        print("Invalid card number")
                        continue
                    if len(card) == 16:
                        print(str(card))
                        cardconfirm = input("Is this the correct card number? ").upper()
                        if cardconfirm == "NO" or cardconfirm == "N":
                            continue
                        if cardconfirm == "YES" or cardconfirm == "Y":
                            taxtotal = total + (total * .0675)
                            print("Your total with tax is $" + format(taxtotal/100,',.2f'))
                            while True:
                                address = input("Please enter your shipping address: ").upper()
                                print(str(address))
                                addressconfirm = input("Is this the correct address? ").upper()
                                if addressconfirm == "NO" or addressconfirm == "N":
                                    continue
                                if addressconfirm == "YES" or addressconfirm == "Y":
                                    print("Thank you for your purchase!")
                                    list.clear(cart)
                                    break
                                break
                            break
                        break
                    break
                break
            break
    #function to show  when an input is not valid
    else:
        print("Invalid Command")